# ubuntu-mate-themes

GTK2, GTK3, Unity and Metacity themes from Ubuntu MATE.

Files URL: http://mirrors.kernel.org/ubuntu/pool/universe/u/ubuntu-mate-artwork/

https://launchpad.net/ubuntu-mate/

https://mirrors.kernel.org/ubuntu/pool/universe/u/ubuntu-mate-artwork/

https://mirrors.edge.kernel.org/ubuntu/pool/universe/u/ubuntu-mate-artwork/

How to clone this repo:

```
git clone https://gitlab.com/rebornos-team/themes-and-icons/ubuntu-mate-themes.git
```

